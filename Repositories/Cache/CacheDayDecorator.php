<?php

namespace Modules\OpeningHours\Repositories\Cache;

use Modules\OpeningHours\Repositories\DayRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDayDecorator extends BaseCacheDecorator implements DayRepository
{
    public function __construct(DayRepository $day)
    {
        parent::__construct();
        $this->entityName = 'openinghours.days';
        $this->repository = $day;
    }
}
