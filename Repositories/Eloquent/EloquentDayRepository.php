<?php

namespace Modules\OpeningHours\Repositories\Eloquent;

use Modules\OpeningHours\Repositories\DayRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDayRepository extends EloquentBaseRepository implements DayRepository
{
}
