<?php

namespace Modules\OpeningHours\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\OpeningHours\Entities\Day;
use Modules\OpeningHours\Http\Requests\CreateDayRequest;
use Modules\OpeningHours\Http\Requests\UpdateDayRequest;
use Modules\OpeningHours\Repositories\DayRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DayController extends AdminBaseController
{
    /**
     * @var DayRepository
     */
    private $day;

    public function __construct(DayRepository $day)
    {
        parent::__construct();

        $this->day = $day;
    }

    public function index() {
        $days = Day::all();
        $noBreak = true;

        foreach ($days as $day)
            if ($day->time3 != null || $day->time4 != null)
                $noBreak = false;

        $daysOffice = Day::allOffice();
        $noBreakOffice = true;

        foreach ($daysOffice as $day)
            if ($day->time3 != null || $day->time4 != null)
                $noBreakOffice = false;

        return view('openinghours::admin.days.index', [
            "days" => $days,
            "noBreak" => $noBreak,

            "daysOffice" => $daysOffice,
            "noBreakOffice" => $noBreakOffice
        ]);
    }

    public function update(Request $request) {
        $noBreak = (isset($request["no-break"]) && $request["no-break"]);
        for ($i = 1; $i <= 7; $i++) {
            $day = Day::where("day_number", $i)->firstOrFail();
            $t1 = $request[$i."-time1"];
            $t2 = $request[$i."-time2"];
            $t3 = $request[$i."-time3"];
            $t4 = $request[$i."-time4"];
            $nonstop = (isset($request[$i."-nonstop"]) && $request[$i."-nonstop"]);

            $day->time1 = $day->time2 = $day->time3 = $day->time4 = null;
            if (!$nonstop) {
                $day->time1 = $t1;
                $day->time2 = $t2;
                if (!$noBreak) {
                    $day->time3 = $t3;
                    $day->time4 = $t4;
                }
            }
            $day->save();
        }

        // Office hours
        $noBreakOffice = (isset($request["no-break-office"]) && $request["no-break-office"]);
        for ($i = 8; $i <= 14; $i++) {
            $day = Day::where("day_number", $i)->firstOrFail();
            $t1 = $request[$i."-time1"];
            $t2 = $request[$i."-time2"];
            $t3 = $request[$i."-time3"];
            $t4 = $request[$i."-time4"];
            $nonstop = (isset($request[$i."-nonstop"]) && $request[$i."-nonstop"]);

            $day->time1 = $day->time2 = $day->time3 = $day->time4 = null;
            if (!$nonstop) {
                $day->time1 = $t1;
                $day->time2 = $t2;
                if (!$noBreakOffice) {
                    $day->time3 = $t3;
                    $day->time4 = $t4;
                }
            }
            $day->save();
        }
        
        return redirect()->route('admin.openinghours.day.index')
            ->withSuccess(trans('openinghours::openinghours.saved'));
    }
}
