<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/openinghours'], function (Router $router) {
    $router->bind('day', function ($id) {
        return app('Modules\OpeningHours\Repositories\DayRepository')->find($id);
    });
    $router->get('days', [
        'as' => 'admin.openinghours.day.index',
        'uses' => 'DayController@index',
        'middleware' => 'can:openinghours.days.index'
    ]);
    $router->post('days-update', [
        'as' => 'admin.openinghours.day.update',
        'uses' => 'DayController@update',
        'middleware' => 'can:openinghours.days.index'
    ]);

});
