<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningHoursDayTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openinghours__day_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('day_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['day_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('openinghours__day_translations', function (Blueprint $table) {
            $table->dropForeign(['day_id']);
        });
        Schema::dropIfExists('openinghours__day_translations');
    }
}
