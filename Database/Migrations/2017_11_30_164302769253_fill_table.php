<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        for ($i = 1; $i <= 7; $i++) { 
            DB::table('openinghours__days')->insert(
                [
                    'day_number'    => $i,
                    'time1'         => '9:00',
                    'time2'         => '17:00',
                    'time3'         => null,
                    'time4'         => null,
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasTable('openinghours__days'))
            DB::table('openinghours__days')->truncate();
    }
}
