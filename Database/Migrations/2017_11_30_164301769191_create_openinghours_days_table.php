<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningHoursDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openinghours__days', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->tinyInteger("day_number");
            $table->string("time1", 5)->nullable()->default("9:00");
            $table->string("time2", 5)->nullable()->default("17:00");
            $table->string("time3", 5)->nullable()->default(NULL);
            $table->string("time4", 5)->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('openinghours__days');
    }
}
