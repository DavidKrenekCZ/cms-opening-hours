# Opening hours CMS module

## Installation


### Composer requirement
``` json
composer require davidkrenekcz/openinghours
```

### Package migrations
``` json
php artisan module:migrate OpeningHours
```

## Frontend views
Your settings are available as collection of *Day* class from every view in **$openingHours** variable.

You can use your own template...
``` php
@foreach($openingHours as $h)
	<b>{{ $h->dayName }}:</b> <span>{{ $h->timeGroup1 }}</span> @if($h->timeGroup2) <span>{{ $h->timeGroup2 }}</span> @endif <br>	
	{-- outputs something like <b>Monday:</b> <span>9:00 - 12:00</span> <span>13:00 - 17:00</span> --}
	{-- or something like <b>Tuesday:</b> <span>9:00 - 12:00</span> --}
	{-- or <b>Wednesday:</b> <span>nonstop</span> --}
@endforeach
```

...or use default template system...
``` php
@foreach($openingHours as $day)
	{!! $day !!}
@endforeach
```

...or even already rendered timetable from **$openingHoursRendered** variable.
``` php
{!! $openingHoursRendered !!}
```

Default rendering templates are ready to be styled and used right away in your application. Used element classes are `wrapper`, `dayname`, `timegroup`, `timegroup1`, `timegroup2`, `timegroup-single` and `timegroup-nonstop`, all of them prefixed with `dk-openinghours-`.

An example of pre-rendered template could look like this
``` html
<div class='dk-openinghours-wrapper'>
	<div class='dk-openinghours-day'>
		<span class='dk-openinghours-dayname'>
			Monday
		</span>
		<span class='dk-openinghours-timegroup dk-openinghours-timegroup1 dk-openinghours-timegroup-nonstop dk-openinghours-timegroup-single'>
			nonstop
		</span>
	</div>

	<div class='dk-openinghours-day'>
		<span class='dk-openinghours-dayname'>
			Tuesday
		</span>
		<span class='dk-openinghours-timegroup dk-openinghours-timegroup1 dk-openinghours-timegroup-single'>
			9:00 - 17:00
		</span>
	</div>
	
	<div class='dk-openinghours-day'>
		<span class='dk-openinghours-dayname'>
			Wednesday
		</span>
		<span class='dk-openinghours-timegroup dk-openinghours-timegroup1'>
			9:00 - 17:00
		</span>
		<span class='dk-openinghours-timegroup dk-openinghours-timegroup2'>
			18:00 - 20:00
		</span>
	</div>

	<!-- ...and so on... -->

</div>
```

Using `{!! $day !!}` will use the same template but without the wrapping div.