<?php

namespace Modules\OpeningHours\Entities;

use Illuminate\Database\Eloquent\Model;

class DayTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'openinghours__day_translations';
}
