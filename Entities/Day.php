<?php

namespace Modules\OpeningHours\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Day extends Model {
    use Translatable;

    protected $table = 'openinghours__days';
    protected $attributes = ["dayName", "timeGroup1", "timeGroup2"];
    public $timestamps = false; 
    public $translatedAttributes = [];
    protected $fillable = [];

    public function isNonstop() {
    	return ($this->time1 == null && $this->time2 == null && $this->time3 == null && $this->time4 == null);
    }

    public function getDayNameAttribute() {
    	return trans("openinghours::days.".($this->day_number <= 7 ? $this->day_number : $this->day_number-7));
    }

    public function getTimeGroup1Attribute() {
    	if ($this->isNonstop())
    		return "nonstop";
    	return $this->time1." - ".$this->time2;
    }

    public function getTimeGroup2Attribute() {
    	if ($this->time3 != null && $this->time4 != null)
			return $this->time3." - ".$this->time4;
		return false;
    }

    public function __toString() {
    	return (
    		"<div class='dk-openinghours-day'>\n".
    			"<span class='dk-openinghours-dayname'>\n".
    				$this->dayName.
    			"\n</span>\n".
    			"<span class='dk-openinghours-timegroup dk-openinghours-timegroup1".
    				($this->isNonstop() ? " dk-openinghours-timegroup-nonstop" : "").
    				(!$this->timeGroup2 ? " dk-openinghours-timegroup-single" : "")."'>\n".
    				$this->timeGroup1.
    			"\n</span>".
    			($this->timeGroup2 ?
    			"\n<span class='dk-openinghours-timegroup dk-openinghours-timegroup2'>\n".
    				$this->timeGroup2.
    			"\n</span>" : "").
    		"\n</div>\n\n"
    	);
    }

    public static function allRendered() {
    	return self::renderCollection(self::all());
    }

    public static function allOfficeRendered() {
        return self::renderCollection(self::allOffice());
    }

    public static function renderCollection($collection) {
        $return = "<div class='dk-openinghours-wrapper'>\n";
        foreach ($collection as $day)
            $return .= $day;
        $return .= "\n</div>";

        return $return;
    }

    public static function all($columns = []) {
        return self::where("day_number", "<=", 7)->get();
    }

    public static function allOffice() {
        return self::where("day_number", ">=", 8)->get();
    }
}
