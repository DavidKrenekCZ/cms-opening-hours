<?php

namespace Modules\OpeningHours\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\OpeningHours\Events\Handlers\RegisterOpeningHoursSidebar;
use Modules\OpeningHours\Composers\OpeningHoursViewComposer;

class OpeningHoursServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterOpeningHoursSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('days', array_dot(trans('openinghours::days')));
            // append translations

        });

        view()->composer("*", OpeningHoursViewComposer::class);
    }

    public function boot()
    {
        $this->publishConfig('openinghours', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\OpeningHours\Repositories\DayRepository',
            function () {
                $repository = new \Modules\OpeningHours\Repositories\Eloquent\EloquentDayRepository(new \Modules\OpeningHours\Entities\Day());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\OpeningHours\Repositories\Cache\CacheDayDecorator($repository);
            }
        );
// add bindings

    }
}
