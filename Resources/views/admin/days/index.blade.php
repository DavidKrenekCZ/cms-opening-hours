@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('openinghours::openinghours.title.openinghours') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('openinghours::openinghours.title.openinghours') }}</li>
    </ol>
@stop

@section('content')
    <form action="{{ route("admin.openinghours.day.update") }}" method="post">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('openinghours::openinghours.title.openinghours') }}</h3>
                    </div>
                    <div class="box-body">
                        <label class="no-break-label">
                            <input type="checkbox" name="no-break" class="i-check"{{ $noBreak ? " checked='checked'" : "" }}> {{ trans("openinghours::days.0") }}
                        </label>
                        <div class="table-responsive">
                            <table class="table hours-table">
                                <tbody>
                                    @foreach($days as $day)
                                    <tr>
                                        <td>{{ $day->dayName }}</td>
                                        <td>
                                            <input type="text" name="{{ $day->day_number }}-time1" value="{{ $day->time1 }}" class="timepicker">
                                            <span class="divider">-</span>
                                            <input type="text" name="{{ $day->day_number }}-time2" value="{{ $day->time2 }}" class="timepicker">
                                        </td>
                                        <td>
                                            <input type="text" name="{{ $day->day_number }}-time3" value="{{ $day->time3 }}" class="timepicker">
                                            <span class="divider">-</span>
                                            <input type="text" name="{{ $day->day_number }}-time4" value="{{ $day->time4 }}" class="timepicker">
                                        </td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="{{ $day->day_number }}-nonstop" class="i-check"{{ $day->isNonstop() ? " checked='checked'" : "" }}> Nonstop
                                            </label>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
    
            <div class="col-md-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('openinghours::openinghours.title.officehours') }}</h3>
                    </div>
                    <div class="box-body">
                        <label class="no-break-label">
                            <input type="checkbox" name="no-break-office" class="i-check"{{ $noBreakOffice ? " checked='checked'" : "" }}> {{ trans("openinghours::days.0") }}
                        </label>
                        <div class="table-responsive">
                            <table class="table hours-table">
                                <tbody>
                                    @foreach($daysOffice as $day)
                                    <tr>
                                        <td>{{ $day->dayName }}</td>
                                        <td>
                                            <input type="text" name="{{ $day->day_number }}-time1" value="{{ $day->time1 }}" class="timepicker">
                                            <span class="divider">-</span>
                                            <input type="text" name="{{ $day->day_number }}-time2" value="{{ $day->time2 }}" class="timepicker">
                                        </td>
                                        <td>
                                            <input type="text" name="{{ $day->day_number }}-time3" value="{{ $day->time3 }}" class="timepicker">
                                            <span class="divider">-</span>
                                            <input type="text" name="{{ $day->day_number }}-time4" value="{{ $day->time4 }}" class="timepicker">
                                        </td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="{{ $day->day_number }}-nonstop" class="i-check"{{ $day->isNonstop() ? " checked='checked'" : "" }}> Nonstop
                                            </label>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div class="text-center">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-success btn-flat" value="{{ trans("openinghours::openinghours.save") }}">
            </div>
        </div>
    </form>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('openinghours::days.title.create day') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script>@include("openinghours::timepicker.js")</script>
    <script>
        // http://jdewit.github.io/bootstrap-timepicker/
        $(".hours-table .timepicker").timepicker({
            showInputs: false,
            minuteStep: 15,
            defaultTime: false,
            showMeridian: false,
        });

        $(".i-check").on("ifChanged", function() {
            showHideElements($(this).closest(".box-body").find(".hours-table"));
        });

        $(".i-check").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
        });

        function showHideElements(hoursTable) {
            // no break
            hoursTable.find("td:nth-child(3)").toggleClass("hide", hoursTable.closest(".box-body").find("input[type='checkbox']")[0].checked);

            hoursTable.find("input[type='checkbox']").each(function() {
                var isNonstop = this.checked;
                if (isNonstop)
                    $(this).closest("tr").find("input.timepicker").attr("disabled", "disabled");
                else
                    $(this).closest("tr").find("input.timepicker").removeAttr("disabled");
            });
        }

        $(".hours-table").each(function() {
            showHideElements($(this));
        });
    </script>
@endpush

@push("css-stack")
    <style type="text/css">@include("openinghours::timepicker.css")</style>
    <style type="text/css">
        .no-break-label {
            display: block;
            text-align: center;
        }

        .no-break-label>div {
            margin-right: .5rem;
        }

        .hours-table .timepicker {
             width: 45% !important;
        }

        .hours-table td {
            vertical-align: middle !important;
        }

        .hours-table .timepicker {
            padding: 3px 0;
            text-align: center;
            border-radius: 3px;
            border: 1px solid #ccc;
            color: black;
        }

        .hours-table .divider {
            color: black;
            font-weight: bold;
            transform: scaleX(2);
            display: inline-block;
            width: 5%;
            text-align: center;
        }

        .hours-table label {
            font-weight: 500;
            margin-bottom: 0;
            min-width: 8rem;
        }

        .hours-table label .icheckbox_flat-blue {
            transform: scale(.75);
            margin-right: .2rem;
        }
    </style>
@endpush