<?php
return [
	"list resource" => "Přístup k otevírací době",
	"title" => [
		"openinghours" => "Otevírací doba",
		"officehours" => "Úřední hodiny"
	],
	"save" => "Uložit",
	"saved" => "Otevírací doba úspěšně uložena!"
];