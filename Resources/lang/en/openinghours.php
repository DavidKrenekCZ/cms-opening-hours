<?php
return [
	"list resource" => "Opening hours access",
	"title" => [
		"openinghours" => "Opening hours",
		"officehours" => "Office hours"
	],
	"save" => "Save",
	"saved" => "Opening hours successfully saved"
];